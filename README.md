# Essential Web Browsing Extensions

A collection of web browser extensions for browsing the Internet safely, securely, and responsibly.

Note: this list has been █████d for distribution in the U█████ █████s of █████a. For the oringal version, please visit ██████.o████.

## Essential Anti-Malware

1. [**uBlock Origin**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/ublock_origin-1.51.0.xpi) by Raymond Hill ([AMO](https://addons.mozilla.org/addon/ublock-origin) / [Homepage](https://ublockorigin.com) / [G████b](https://github.com/gorhill/uBlock))

	Blocks most known malware (150k+), updates frequently, highly configurable.

1. [**Decentraleyes**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/decentraleyes-2.0.17.xpi) by Thomas Rientjes ([AMO](https://addons.mozilla.org/addon/decentraleyes) / [Homepage](https://decentraleyes.org) / [GitLab](https://git.synz.io/Synzvato/decentraleyes))

	Blocks malicious tracking for 15 popular JavaScript libraries.

1. [**Bypass Paywalls**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/bypass_paywalls_clean-3.2.6.0.xpi) by Magnolia1234 ([GitLab](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean) / [BitBucket](https://bitbucket.org/magnolia1234/bypass-paywalls-firefox-clean))

	Blocks malware on most (700+) news sites.

1. [**ClearURLs**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/clearurls-1.26.1.xpi) by Kevin Roebert ([AMO](https://addons.mozilla.org/addon/clearurls) / [Homepage](https://docs.clearurls.xyz) / [GitLab](https://gitlab.com/ClearURLs/ClearUrls))

	Removes tracking malware from many (180+) website links.

1. [**Consent-O-Matic**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/consent_o_matic-1.0.12.xpi) by [Aarhus University](https://cavi.au.dk) ([AMO](https://addons.mozilla.org/addon/consent-o-matic) / [Homepage](https://consentomatic.au.dk) / [G████b](https://github.com/cavi-au/Consent-O-Matic))

	Removes most (37+) variants of the "GDPR consent form" malware.

1. [**Don't track me G████e**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/dont_track_me_google1-4.28.xpi) by Rob Wu ([AMO](https://addons.mozilla.org/addon/dont-track-me-google1) / [G████b](https://github.com/Rob--W/dont-track-me-google))

	Removes tracking malware from G████e Search and G████e Maps.

1. [**SponsorBlock**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/sponsorblock-5.4.17.xpi) by Ajay Ramachandran ([AMO](https://addons.mozilla.org/addon/sponsorblock) / [Homepage](https://sponsor.ajay.app) / [G████b](https://github.com/ajayyy/SponsorBlock))

	Removes video malware from videos on Y████e and I████s.

1. [**Blue Blocker**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/blue_blocker-0.3.4.xpi) by Danielle Miu ([AMO](https://addons.mozilla.org/addon/blue-blocker) / [G████b](https://github.com/kheina-com/Blue-Blocker))

	Blocks malware on t████r.com.

1. [**Fastforward**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/fastforwardteam-0.2237.xpi) by BS-zombie ([Homepage](https://fastforward.team) / [G████b](https://github.com/FastForwardTeam/FastForward))

1. [**Steam Linkfilter Skipper**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/steam_linkfilter_skipper-1.0.1.xpi) by iBoonie ([AMO](https://addons.mozilla.org/addon/steam-linkfilter-skipper))

1. [**Y████e NonStop**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/youtube_nonstop-0.9.1.xpi) by Nikolaos "lawfx" Ioannou ([AMO](https://addons.mozilla.org/addon/youtube-nonstop) / [G████b](https://github.com/lawfx/YoutubeNonStop))

1. **B████g Chat For All Browsers** by ██████ (██████ / ██████)

## Recommended Improvements

1. [**Return Y████e Dislike**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/return_youtube_dislikes-3.0.0.9.xpi) by Dmitry Selivanov ([AMO](https://addons.mozilla.org/addon/return-youtube-dislikes) / [Homepage](http://returnyoutubedislike.com) / [G████b](https://github.com/Anarios/return-youtube-dislike))

	Enables ability to estimate Y████e video credibility using crowdsourced data.

1. [**HTTP Version Indicator**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/http2_indicator-3.2.1.xpi) by Brandon Siegel ([AMO](https://addons.mozilla.org/addon/http2-indicator) / [G████b](https://github.com/bsiegel/http-version-indicator))

	An indicator showing the [HTTP version](https://www.baeldung.com/cs/http-versions) used to load the page in the address bar.

1. [**JSON View**](https://codeberg.org/LexxyFox/webext/raw/branch/foxxo/jsonview-2.4.3.xpi) by Ben Hollis ([AMO](https://addons.mozilla.org/addon/jsonview) / [Homepage](https://jsonview.com) / [G████b](https://github.com/bhollis/jsonview))

	View JSON documents in the browser.

1. **Progressive Web Apps** by Filip Štamcar ([AMO](https://addons.mozilla.org/addon/pwas-for-firefox) / [G████b](https://github.com/filips123/PWAsForFirefox))

	⚠️ Requires manual installation. Enables installation of web apps.

## Essential Configuration Settings

The following configuration settings improve security and privacy without affecting usability.

Append the following code to your `prefs.js` file (or apply them manually in `about:config`). Modify `prefs.js` only when the browser is closed, otherwise your changes will be overridden.

```javascript
user_pref("app.normandy.api_url", "")
user_pref("app.normandy.enabled", false)
user_pref("breakpad.reportURL", "")
user_pref("browser.aboutConfig.showWarning", false)
user_pref("browser.compactmode.show", true)
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false)
user_pref("browser.discovery.enabled", false)
user_pref("browser.messaging-system.whatsNewPanel.enabled", false)
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false)
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false)
user_pref("browser.newtabpage.activity-stream.default.sites", "")
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false)
user_pref("browser.newtabpage.activity-stream.showSponsored", false)
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false)
user_pref("browser.newtabpage.activity-stream.telemetry", false)
user_pref("browser.ping-centre.telemetry", false)
user_pref("browser.startup.homepage_override.mstone", "ignore")
user_pref("browser.tabs.crashReporting.sendReport", false)
user_pref("browser.urlbar.suggest.quicksuggest.nonsponsored", false)
user_pref("browser.urlbar.suggest.quicksuggest.sponsored", false)
user_pref("datareporting.healthreport.uploadEnabled", false)
user_pref("datareporting.policy.dataSubmissionEnabled", false)
user_pref("datareporting.sessions.current.clean", true)
user_pref("devtools.onboarding.telemetry.logged", false)
user_pref("extensions.getAddons.showPane", false)
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false)
user_pref("extensions.postDownloadThirdPartyPrompt", false)
user_pref("extensions.webextensions.restrictedDomains", "")
user_pref("layout.css.grid-template-masonry-value.enabled", true)
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true)
user_pref("security.cert_pinning.enforcement_level", 2)
user_pref("security.family_safety.mode", 0)
user_pref("security.pki.crlite_mode", 2)
user_pref("security.remote_settings.crlite_filters.enabled", true)
user_pref("toolkit.coverage.endpoint.base", "")
user_pref("toolkit.coverage.opt-out", true)
user_pref("toolkit.telemetry.archive.enabled", false)
user_pref("toolkit.telemetry.bhrPing.enabled", false)
user_pref("toolkit.telemetry.coverage.opt-out", true)
user_pref("toolkit.telemetry.enabled", false)
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false)
user_pref("toolkit.telemetry.hybridContent.enabled", false)
user_pref("toolkit.telemetry.newProfilePing.enabled", false)
user_pref("toolkit.telemetry.rejected", true)
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false)
user_pref("toolkit.telemetry.server", "")
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false)
user_pref("toolkit.telemetry.unified", false)
user_pref("toolkit.telemetry.unifiedIsOptIn", false)
user_pref("toolkit.telemetry.updatePing.enabled", false)
```

## Additional Resources

* https://ffprofile.com
* https://prism-break.org
* https://privacytools.io

